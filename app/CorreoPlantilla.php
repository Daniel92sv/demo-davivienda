<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CorreoPlantilla extends Model
{
    protected $table = 'correo_plantilla';
}
