<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConvenioTipo extends Model
{
    protected $table = 'convenio_tipo';
}
