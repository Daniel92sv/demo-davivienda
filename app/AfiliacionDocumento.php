<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AfiliacionDocumento extends Model
{
    protected $table = 'afiliacion_documento';
}
