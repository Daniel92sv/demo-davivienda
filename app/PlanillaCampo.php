<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PlanillaCampo extends Model
{
    protected $table = 'planilla_campo';
}
