<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LibranzaDocumento extends Model
{
    protected $table = 'libranza_documento';
}
