<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LibranzaComentario extends Model
{
    protected $table = 'libranza_comentario';
}
