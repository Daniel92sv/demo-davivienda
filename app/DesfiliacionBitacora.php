<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DesfiliacionBitacora extends Model
{
    protected $table = 'desafiliacion_bitacora';
}
