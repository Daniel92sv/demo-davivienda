<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ConvenioDocumento extends Model
{
    protected $table = 'convenio_documento';
}
