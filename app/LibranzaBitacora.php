<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LibranzaBitacora extends Model
{
    protected $table = 'libranza_bitacora';
}
